package be.multimedi.graphics.lotto;

public class Main {
    public static void main(String[] args) {
        Lotto lotto = new Lotto();

        System.out.println(Lotto.NUMBER_OF_BALLS);

        int coins = lotto.play(20);

        System.out.printf("you have %d coins", coins);

    }
}
