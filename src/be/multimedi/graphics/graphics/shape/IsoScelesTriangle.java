package be.multimedi.graphics.graphics.shapes;

import be.multimedi.graphics.graphics.shape.Triangle;

/**
 * @author Sven Wittoek
 * created on Thursday, 20/01/2022
 */
public class IsoSeclesTriangle extends Triangle {
    private static int count = 0;

    public IsoSeclesTriangle(int x, int y, int w, int h){
        this.setX(x);
        this.setY(y);
        this.setWidth(w);
        this.setHeight(h);
    }

    public IsoSeclesTriangle(){
        this(0,0,0,0);
    }

    public IsoSeclesTriangle(int w, int h){
        this(0,0,w,h);
    }

    public IsoSeclesTriangle(IsoSeclesTriangle copy){
        this(copy.getX(), copy.getY(), copy.width, copy.height);
    }

    public static int getCount() {
        return count;
    }

    @Override
    public void setWidth(int width){
        if(width < 0) width = -width;
        this.width = width;
        this.perpendicular = width /2;
    }

    @Override
    public void setPerpendicular(int perpendicular){
        if( perpendicular < 0 ) perpendicular = -perpendicular;
        this.perpendicular = perpendicular;
        this.width = perpendicular * 2;
    }


}
