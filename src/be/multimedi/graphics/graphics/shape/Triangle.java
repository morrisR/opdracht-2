package be.multimedi.graphics.graphics.shape;
import be.multimedi.graphics.graphics.Shape;
import be.multimedi.graphics.graphics.drawings.DrawingContext;

/**
 * @author Sven Wittoek
 * created on Thursday, 20/01/2022
 */
public class Triangle extends Shape {
    private static int count = 0;
    protected int width;
    protected int height;
    protected int perpendicular;

    public Triangle() {

    }

    public Triangle(int width, int height, int perpendicular) {
        this(width, height, perpendicular, 0, 0);
    }

    public Triangle(int width, int height, int perpendicular, int x, int y) {
        count++;
        setWidth(width);
        setHeight(height);
        setPerpendicular(perpendicular);
        setPosition(x, y);
    }

    public Triangle(Triangle t) {
        this(t.width, t.height, t.perpendicular, t.getX(), t.getY());
    }

    public static int getCount() {
        return count;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width < 0 ? -width : width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height < 0 ? -height : height;
    }

    public int getPerpendicular() {
        return perpendicular;
    }

    public void setPerpendicular(int perpendicular) {
        this.perpendicular = perpendicular < 0 ? -perpendicular : perpendicular;
    }

    @Override
    public double getArea() {
        return width * height / 2;
    }

    @Override
    public double getPerimeter() {
        int temp = width - perpendicular;
        return Math.sqrt(perpendicular * perpendicular + height * height)
                + Math.sqrt(temp * temp + height * height) + width;
    }

    @Override
    public void grow(int d) {
        setWidth(width + d);
        setHeight(height + d);
    }

    @Override
    public void scale(int scaleFactor) {
        setHeight((int) (height * (scaleFactor / 100f)));
        setWidth((int) (width * (scaleFactor / 100f)));
        setPerpendicular((int) (perpendicular * (scaleFactor / 100f)));
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "width=" + width +
                ", height=" + height +
                ", perpendicular=" + perpendicular +
                ", x=" + getX() +
                ", y=" + getY() +
                '}';
    }

}
