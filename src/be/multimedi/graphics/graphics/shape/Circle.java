package be.multimedi.graphics.graphics.shapes;

import be.multimedi.graphics.graphics.Shape;
import be.multimedi.graphics.graphics.drawings.DrawingContext;

/**
 * @author Sven Wittoek
 * created on Thursday, 20/01/2022
 */
public class Circle extends Shape {
    private static int count = 0;
    private int radius;

    public Circle() {
        this(0);
    }

    public Circle(int radius) {
        this(radius, 0, 0);
    }

    public Circle(int radius, int x, int y) {
        count++;
        setRadius(radius);
        setPosition(x, y);
    }

    public Circle(Circle c) {
        this(c.radius, c.getX(), c.getY());
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius < 0 ? -radius : radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }


    @Override
    public void grow(int d) {
        setRadius(radius + d);
    }

    public static int getCount() {
        return count;
    }

    @Override
    public void scale(int scaleFactor) {
        setRadius((int) (radius * (scaleFactor / 100f)));
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

}
