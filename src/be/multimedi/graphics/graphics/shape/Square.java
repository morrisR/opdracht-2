package be.multimedi.graphics.graphics.shapes;

/**
 * @author Sven Wittoek
 * created on Tuesday, 18/01/2022
 */
public final class Square extends Rectangle {
    public static final int ANGLES = 6;
    private static int squareCount;

    {
        squareCount++;
    }

    public Square() {
    }

    public Square(int side) {
        super(side, side);
    }

    public Square(int side, int x, int y){
        super(side, side, x, y);
    }

    public Square(Square square) {
        super(square);
    }

    public static int getCount() {
        return squareCount;
    }

    @Override
    public void setHeight(int height){
        setSide(height);
    }

    @Override
    public void setWidth(int width) {
        setSide(width);
    }

    public void setSide(int side) {
        super.setWidth(side);
        super.setHeight(side);
    }

    public int getSide() {
        return getHeight();
    }

}
