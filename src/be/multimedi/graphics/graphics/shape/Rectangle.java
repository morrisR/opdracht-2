package be.multimedi.graphics.graphics.shapes;

import be.multimedi.graphics.graphics.Shape;
import be.multimedi.graphics.graphics.drawings.DrawingContext;

/**
 * @author Sven Wittoek
 * created on Monday, 17/01/2022
 */
public class Rectangle extends Shape {
    public static final int ANGLES = 4;
    private static int rectangleCount;
    private int width;
    private int height;

    {
        rectangleCount++;
    }

    public Rectangle() {
        this(5, 10);
    }

    public Rectangle(int width, int height) {
        this(width, height, 0, 0);
    }

    public Rectangle(int width, int height, int x, int y) {
        super(x, y);
        setWidth(width);
        setHeight(height);
    }

    public Rectangle(Rectangle rectangle) {
        this(rectangle.width, rectangle.height, rectangle.getX(), rectangle.getY());
    }

    public static int getCount() {
        return rectangleCount;
    }

    public void setWidth(int width) {
        this.width = Math.abs(width);
    }
    public int getWidth() {
        return width;
    }
    public void setHeight(int height) {
        this.height = Math.abs(height);
    }
    public int getHeight() {
        return height;
    }
    public double getArea() {
//        TODO add the right calculation for the Area
        return 0;
    }
    public double getPerimeter() {
        return (width + height) * 2;
    }
    public void grow(int growthFactor) {
        setHeight(height * growthFactor);
        setWidth(width * growthFactor);
    }

    @Override
    public void scale(int scaleFactor) {
        setHeight((int) (height * (scaleFactor / 100F)));
        setWidth((int) (width * (scaleFactor / 100F)));
    }

    @Override
    public void draw(DrawingContext drawingContext) {
        drawingContext.draw(this);
    }

    @Override
    public String toString() {
        return String.format("This is a rectangle with a height of %d, a width of %d %n" +
                "and a position of %d:%d%n", getHeight(), getWidth(), getX(), getY());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null && !(o instanceof Rectangle)) {
            return false;
        }
        Rectangle rect = (Rectangle) o;
        return this.getWidth() == rect.getWidth() &&
                this.getHeight() == rect.getHeight() &&
                this.getX() == rect.getX() &&
                this.getY() == rect.getY();
    }

    @Override
    public int hashCode() {
        return width * 8 + height * 13 + getX() * 21
                + getY() * 34;
    }
}
