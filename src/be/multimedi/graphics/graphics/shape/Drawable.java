package be.multimedi.graphics.graphics.shape;

import com.sun.xml.internal.bind.v2.TODO;

public interface Drawable extends Scaleable{

    public abstract void draw(DrawingContext dc);
}
