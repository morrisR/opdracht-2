package be.multimedi.graphics.graphics.animal;

public class Cat extends Animal {

    public Cat() {

    }

    public Cat(String name) {
        setName(name);


    }

    public void move() {
        System.out.println("a cat walks");

    }

    public void makeNice() {
        System.out.println("meow ");
    }
}
