package be.multimedi.graphics.graphics.animal;

public abstract class  Animal {
    private String name;
    public Animal(){

    }
    public Animal(String name){
        this.name=name;
    }
    public void setName (String isName){
        name=isName;
    }
    public String getName() {
        return name;
    }
    public abstract void move();
    public abstract void makeNice();
}
