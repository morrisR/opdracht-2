package be.multimedi.graphics.graphics;

import be.multimedi.graphics.graphics.shape.*;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Monday, 17/01/2022
 */
public class Main {
    public static void main(String[] args) {
        Square square = new Square();
        Rectangle rectangle = new Rectangle ();
        Circle circle = new Circle(15, 3, 1);
        IsoScelesTriangle isoScelesTriangle = new IsoScelesTriangle(2,10,1,2);
        Triangle triangle = new Triangle();
        printr(square);
        printr(rectangle);
        printr(circle);
        printr(isoScelesTriangle);
        printr(triangle);

    }
    private static void printr(Triangle triangle) {
        System.out.printf("this is a triangle or isoScelesTriangle with height %d and width %d on %d:%d%n", triangle.getHeight(), triangle.getWidth(), triangle.getX(), triangle.getY());

    }
    private static void printr(Circle circle) {
        System.out.printf("this is a circle  with radius van %d  on %d:%d%n", circle.getRadius(),  circle.getX(), circle.getY());

    }
    private static void printr(Rectangle rectangle) {
        System.out.printf("this is a rectangle or square with height %d and width %d on %d:%d%n", rectangle.getHeight(), rectangle.getWidth(), rectangle.getX(), rectangle.getY());

    }
}
