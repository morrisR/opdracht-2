package be.multimedi.graphics.graphics.enums;

public enum Coins {

    MAANDAG(1),
    DINSDAG(2),
    WOENSDAG(3),
    DONDERDAG(4),
    VRIJDAG(5),
    ZATERDAG(6),
    ZONDAG(7);
    private double waarde;

    Coins(double waarde) {
        this.waarde = waarde;
    }

}
