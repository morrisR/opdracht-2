package be.multimedi.graphics.graphics.enums;

public enum Days {

    MAANDAG(1),
    DINSDAG(2),
    WOENSDAG(3),
    DONDERDAG(4),
    VRIJDAG(5),
    ZATERDAG(6),
    ZONDAG(7);
    private int day;

    Days(int day) {
        this.day = day;
    }

    public int getDay() {
        return day;
    }

    public boolean weekday() {
        return day < 6;
    }
}

